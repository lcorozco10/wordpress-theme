<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wpsinterdb');

/** MySQL database username */
define('DB_USER', 'luis');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5c~PP<3ju|4)TJAuYm*x<.spG)LZ]dG-t(Y>tG8|eV/;2WJuR@Q-.j8Ja[Ql|as2');
define('SECURE_AUTH_KEY',  'vNE_imp|dRR=aXUa][5Kl9Qg|M4O88_}1Q$NUkKE*t-6WFUiEY&T:{-N7WGI2OdU');
define('LOGGED_IN_KEY',    'k$:)Zf$bo9{/{p|-A;4ck[$|bnqzz9<Vq`Y&4Z+Z/qe^w&8|hIQi+5RX+!oh7h}f');
define('NONCE_KEY',        'N+pVbC$-MTsvw*W(|O8``x2?,FU1T00ymO!R?pZF1dcElA?#H-tukGI+b00%Sw}|');
define('AUTH_SALT',        'M2kD/dB/rTO|+fChp-oqbGlrfku{MgCiY9#2Zo6={XgDJ627,@Zpn1a)LK}.yQ(l');
define('SECURE_AUTH_SALT', '0h:-MWcJ N+8fBHd62o*DhpIT/yV0-m5#R,p+V4!-P@@#)C+tGZl%OQ|k_4mYoJ_');
define('LOGGED_IN_SALT',   '$I2X?{ $&3G|]^@>^EWa{MLx+>0L*I*.D9jbT,0bV>_?=U % J}m4<]CoJ2dq^;S');
define('NONCE_SALT',       'RVC!yu#7+-GWR|FuC+bfF.p%tUbs? 8+GKdp%@J)P@-mlK3ynGk@q|:m&fKT{d96');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
